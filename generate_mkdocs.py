import os
import yaml

def generate_nav(directory):
    nav = []

    for root, dirs, files in os.walk(directory):
        # Skip root directory itself for proper nesting
        if root == directory:
            continue
        
        # Build relative path
        rel_path = os.path.relpath(root, directory)
        
        # Collect markdown files
        md_files = [file for file in files if file.endswith('.md')]
        
        # Skip directories without markdown files
        if not md_files:
            continue
        
        # Sort files to ensure consistent order
        md_files.sort()
        
        # Build nav entry for each directory
        nav_entry = {
            rel_path: [
                {file[:-3]: os.path.join(rel_path, file)} for file in md_files
            ]
        }
        nav.append(nav_entry)
    
    # Handle top-level markdown files
    top_level_files = [
        {file[:-3]: file} for file in os.listdir(directory) if file.endswith('.md')
    ]
    if top_level_files:
        nav = top_level_files + nav

    return nav

def main():
    docs_directory = 'docs'
    mkdocs_config = {
        'site_name': 'My Project Documentation',
        'site_description': 'Comprehensive documentation for My Project',
        'plugins': ['techdocs-core'],
        'nav': generate_nav(docs_directory)
    }
    with open('mkdocs.yml', 'w') as f:
        yaml.dump(mkdocs_config, f, default_flow_style=False)

if __name__ == '__main__':
    main()
